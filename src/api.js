const express = require("express");
const cors = require("cors");

const { createUserMiddleware, deleteUserMiddleware } = require("./middlewares/user.js");
const { authMiddlewareFactory } = require("./middlewares/authentication.js");
const { mongoMiddlewareFactory, publishEventMiddlewareFactory } = require("./middlewares/external.js");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  const authMiddleware = authMiddlewareFactory(secret);
  const mongoMiddleware = mongoMiddlewareFactory(mongoClient);
  const publishEventMiddleware = publishEventMiddlewareFactory(stanConn);

  api.get("/", (req, res) => res.json("Hello, World!"));

  api.post("/users", mongoMiddleware, createUserMiddleware, publishEventMiddleware);
  api.delete("/users/:uuid", mongoMiddleware, authMiddleware, deleteUserMiddleware, publishEventMiddleware);

  return api;
};
