const jwt = require("jsonwebtoken");

function authMiddlewareFactory(secret) {
  return (req, res, next) => {
    const auth = req.get("Authentication");

    if (!auth) {
      res.status(401);
      res.json({ error: "Access Token not found" });
      return;
    }

    const { uuid } = req.params;
    const token = auth.split(" ")[1];
    const decoded = jwt.verify(token, secret);

    if (uuid !== decoded.id) {
      res.status(403);
      res.json({ error: "Access Token did not match User ID" });
      return;
    }

    next();
  }
}

module.exports = { authMiddlewareFactory };
