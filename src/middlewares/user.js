const { v4: uuid } = require("uuid");

function createUserMiddleware(req, res, next) {
  const id = uuid();
  const { name, email, password } = req.body;

  if (req.collection.findOne({ email })) {
    res.status(409);
    res.json();
    return
  }

  req.event = {
    eventType: "UserCreated",
    entityId: id,
    entityAggregate: { name, email, password }
  };

  res.status(201);
  res.json({ user: { id, name, email } });
  next();
}

function deleteUserMiddleware(req, res, next) {
  const id = req.params.uuid;

  if (!req.collection.findOne({ id })) {
    res.status(404);
    res.json();
    return;
  }

  req.event = {
    eventType: "UserDeleted",
    entityId: id,
    entityAggregate: {},
  };

  res.status(200);
  res.json({ id });
  next();
}

module.exports = { createUserMiddleware, deleteUserMiddleware };
