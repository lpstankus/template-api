function mongoMiddlewareFactory(mongoClient) {
  return (req, res, next) => {
    const db = mongoClient.db("users");
    if (!db) {
      res.status(500);
      return;
    }
    req.collection = db.collection("users");
    next();
  };
}

function publishEventMiddlewareFactory(stanConn) {
  return (req, res, next) => {
    stanConn.publish(req.event);
    next();
  };
}

module.exports = { mongoMiddlewareFactory, publishEventMiddlewareFactory };
